import { useState } from "react";
import { Alert, Button, Col, Container, Row } from "react-bootstrap";
import { useNavigate, useSearchParams } from "react-router-dom";
import UserDataForm from "../components/UserDataForm";
import { UserData, useLogin } from "../hooks/users";

export default function LoginPage() {
	const login = useLogin();
	const nav = useNavigate();
	const [error, setError] = useState<string | null>(null);
	const [params] = useSearchParams();

	const handleSubmit = (e: UserData) => {
		setError(null);
		login({ username: e.username, password: e.password })
			.then(() => nav(params.get('returnUrl') ?? '/highscores'))
			.catch(err => setError(err));
	};

	const actions = [<Button key="register" onClick={() => nav('/register')}>Register</Button>];

	return (
		<Container>
			{error && <Row>
				<Col>
					<Alert variant="danger">{error}</Alert>
				</Col>
			</Row>}

			<Row>
				<Col>
					<UserDataForm
						header="Login"
						submitLabel="Login"
						onSubmit={handleSubmit}
						actions={actions} />
				</Col>
			</Row>
		</Container>
	);
}
