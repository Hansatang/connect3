import { useEffect, useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { Board, Effect, createBoard } from "../board/board";
import BoardView from "../components/BoardView";
import GameTile from "../components/GameCard";
import { useGame } from "../hooks/game";
import { useRequireAuthed } from "../hooks/users";

export default function PlayPage() {
	useRequireAuthed();

	const { game, updateScore, completeGame } = useGame();
	const [board, setBoard] = useState<Board<string> | null>(null);
	const [points, setPoints] = useState<number>(0);
	const nav = useNavigate();

	const handleComplete = () => completeGame().then(() => nav('/highscores'));

	const handleEvent = (e: Effect<string>) => {
		if(e.match)
			setPoints(points + (e.match.matched.charCodeAt(0) - 65 + 1) * e.match.positions.length);
	};

	useEffect(() => {
		if(!game)
			return;

		updateScore(game.score + points)
			.then(() => setPoints(0));
	}, [board]);

	useEffect(() => {
		setBoard(createBoard({
			next: () => {
				let n = 'Z'.charCodeAt(0) - 'A'.charCodeAt(0);
				n = Math.random() * n;
				n = Math.floor(n);
				n = n + 'A'.charCodeAt(0);
				return String.fromCharCode(n);
			},
		}, 10, 10));
	}, []);

	return (
		<Container className="d-flex flex-column gap-3">
			<Row>
				<Col>
					<GameTile game={game} />
				</Col>
			</Row>

			<Row>
				<Col>
					<Button onClick={() => handleComplete()}>Complete game</Button>
				</Col>
			</Row>

			<Row>
				<Col>
					<BoardView
						board={board}
						onBoardChange={b => setBoard(b)}
						onBoardEvent={handleEvent} />
				</Col>
			</Row>
		</Container>
	);
}
