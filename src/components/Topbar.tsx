import { Container, Nav, Navbar } from "react-bootstrap";
import ProfileButton from "./ProfileButton";
import { Link } from "react-router-dom";

export function Topbar() {
	return (
		<Navbar className="bg-body-tertiary">
			<Container>
				<Navbar.Brand href="/">Match3</Navbar.Brand>

				<Navbar.Toggle />

				<Navbar.Collapse className="justify-content-end">
					<Nav className="me-auto">
						<Nav.Link as={Link} to="/games">Games</Nav.Link>
					</Nav>
					<ProfileButton />
				</Navbar.Collapse>
			</Container>
		</Navbar>
	);
}
