import { useState } from "react";
import { Button, Col, Container, Row } from "react-bootstrap";
import { Board, Effect, Position, canMove, move, piece } from "../board/board";
import { positionEquals } from "../utils";

export type BoardViewProps = {
	board: Board<string> | null,
	onBoardChange: (board: Board<string>) => void,
	onBoardEvent: (effect: Effect<string>) => void,
};

export default function BoardView({ board, onBoardChange, onBoardEvent }: BoardViewProps) {
	const [selected, setSelected] = useState<Position | null>(null);
	const [highlighted, setHighlighted] = useState<string | null>(null);

	const handleSelect = (pos: Position) => {
		if(!board)
			return;

		if(!selected) {
			setSelected(pos);
		} else if(positionEquals(selected, pos)) {
			setSelected(null);
		} else {
			if(canMove(board, selected, pos)) {
				const moveRes = move(null, board, selected, pos);
				moveRes.effects.forEach(e => onBoardEvent(e));
				onBoardChange(moveRes.board);
			} else {
				alert('Invalid move');
			}

			setSelected(null);
		}
	};

	const buttonClasses = (p: Position) => {
		let classes = 'tile';

		if(positionEquals(selected, p))
			classes += ' selected';

		if(!selected && board && piece(board, p) === highlighted)
			classes += ' highlighted';

		if(selected && board && piece(board, p) === piece(board, selected))
			classes += ' highlighted';

		return classes;
	};

	if(!board)
		return <></>;

	return (
		<Container className="board-view">
			{Array.from({ length: board.height }).map((_, row) => <Row key={row}>
				{Array.from({ length: board.width }).map((_, col) => <Col key={col} className="p-0">

					<Button
						className={buttonClasses({ row, col })}
						onClick={() => handleSelect({ row, col })}
						onMouseEnter={() => setHighlighted(piece(board, { row, col })!)}>

						{piece(board, { row, col })}
					</Button>

				</Col>)}
			</Row>)}
		</Container>
	);
}
