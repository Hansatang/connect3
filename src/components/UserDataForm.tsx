import { Button, Card, FloatingLabel, Form, Stack } from "react-bootstrap";
import { useAppSelector } from "../hooks/hooks";
import { useEffect, useState } from "react";
import { User } from "../domain";
import { UserData } from "../hooks/users";

export type UserDataFormProps = {
	header: string,
	onSubmit: (userData: UserData) => void,
	user?: User | null,
	submitLabel?: string
	actions?: JSX.Element[]
};

export default function UserDataForm({ header, onSubmit, user, submitLabel, actions }: UserDataFormProps) {

	const isAdmin = useAppSelector(s => s.auth.user?.admin);

	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const [admin, setAdmin] = useState(false);

	const handleSubmit = (e: React.FormEvent) => {
		e.preventDefault();

		onSubmit({ username, password, admin });
	};

	useEffect(() => {
		setUsername(user?.username ?? '');
		setPassword(user?.password ?? '');
		setAdmin(user?.admin ?? false);
	}, [user]);

	return (
		<Card>
			<Card.Header>{ header }</Card.Header>

			<Card.Body>
				<Form onSubmit={handleSubmit}>
					<FloatingLabel controlId="usernameControl" label="Username" className="mb-3" >

						<Form.Control
							type="text"
							placeholder="Username"
							value={username}
							onChange={e => setUsername(e.target.value)} />
					</FloatingLabel>

					<FloatingLabel
						controlId="passwordControl"
						label="Password"
						className="mb-3" >

						<Form.Control
							type="password"
							placeholder="Password"
							value={password}
							onChange={e => setPassword(e.target.value)} />
					</FloatingLabel>

					{isAdmin && <Form.Check // prettier-ignore
						type="switch"
						id="isAdminControl"
						label="is admin"
						checked={admin}
						onChange={e => setAdmin(e.target.checked)} />}

					<Stack direction="horizontal">
						<Button variant="primary" type="submit" className="me-auto">
							{ submitLabel ?? 'Submit' }
						</Button>

						{actions}
					</Stack>
				</Form>
			</Card.Body>
		</Card>
	);
}
