import { Card, Col, Container, Row } from "react-bootstrap";
import { Game } from "../domain";

export type GameTileProps = {
	game: Game | null,
};

export default function GameTile({ game }: GameTileProps) {
	return (
		<Card body>
			<Container>
				<Row>
					<Col>Id: {game?.id}</Col>
					<Col className="text-center">Score: {game?.score}</Col>
					<Col className="text-center">Completed: {game?.completed ? 'YES' : 'NO'}</Col>
				</Row>
			</Container>
		</Card>
	);
}
