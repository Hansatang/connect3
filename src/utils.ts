import { Position } from "./board/board";

export const okOrReject = (resp: Response) => resp.ok
	? resp
	: resp.text().then(err => Promise.reject(err));

export const positionEquals = (p1: Position | null, p2?: Position | null) => p1?.row === p2?.row && p1?.col === p2?.col;
