
export default interface Game {
	id: string;
	user: string;
	score: number;
	completed: boolean;
}
