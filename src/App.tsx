import { Stack } from 'react-bootstrap';
import { Outlet } from 'react-router-dom';
import './App.css';
import { Topbar } from './components/Topbar';

function App() {
	return (
		<Stack gap={3}>
			<Topbar />
			<Outlet />
		</Stack>
	);
}

export default App;
