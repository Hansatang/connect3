import { useCallback, useEffect, useState } from "react";
import { useHref, useLocation, useNavigate } from "react-router-dom";
import { User } from "../domain";
import env from "../environment";
import { login, logout, updateProfile } from "../redux/authSlice";
import { okOrReject } from "../utils";
import { useAppDispatch, useAppSelector } from "./hooks";

export type UserData = {
	username: string,
	password: string,
	admin: boolean,
};

export type LoginData = {
	username: string,
	password: string,
};

export function useCreateUser(): (cred: UserData) => Promise<User> {

	return (cred: UserData) => fetch(`${env.apiBaseUrl}/users`, {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify(cred),
	})
		.then(okOrReject)
		.then(r => r.json());
}

export function useAuthedFetch(): (url: string, init?: RequestInit) => Promise<Response> {
	const token = useAppSelector(s => s.auth.token);

	return useCallback((url: string, init?: RequestInit) => {
		if(!token)
			return Promise.reject('Not logged in');

		const sep = url.includes('?') ? '&' : '?';
		return fetch(`${url}${sep}token=${token}`, init);
	}, [token]);
}

export function useRequireAuthed(redirectUrl?: string) {
	const nav = useNavigate();
	const user = useAppSelector(s => s.auth.user);
	const loc = useLocation();
	const returnUrl = useHref(loc);

	useEffect(() => {
		if(!user)
			nav(redirectUrl ?? `/login?returnUrl=${returnUrl}`);

	}, [user, nav, redirectUrl, returnUrl]);
}

export function useUser(userId?: string) {
	const [user, setUser] = useState<User | null>(null);
	const [error, setError] = useState<string | null>(null);
	const authedFetch = useAuthedFetch();
	const auth = useAppSelector(s => s.auth);

	const id = userId ?? auth.user?.id;

	const updateUser = useCallback((updated: Partial<User>) => {
		if(!id)
			setError('Not logged in');

		setError(null);
		authedFetch(`${env.apiBaseUrl}/users/${id}`, {
			method: 'PATCH',
			body: JSON.stringify(updated),
		})
			.then(() => setUser(u => u ? { ...u, ...updated } : null))
			.catch(err => setError(err));
	}, [authedFetch, id]);

	useEffect(() => {
		if(!id)
			setUser(null);

		if(id === auth.user?.id) {
			setUser(auth.user);
			return;
		}

		authedFetch(`${env.apiBaseUrl}/users/${id}`)
			.then(okOrReject)
			.then(r => r.json())
			.then(user => setUser(user))
			.catch(err => setError(err))
	}, [authedFetch, id, auth.user])

	return { user, error, updateUser };
}

export function useLogin(): (cred: LoginData) => Promise<User> {
	const dispatch = useAppDispatch();

	return useCallback((cred: LoginData) => {
		return fetch(`${env.apiBaseUrl}/login`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(cred),
		})
			.then(okOrReject)
			.then(r => r.json())
			.then(({ userId, token }) => {
				return fetch(`${env.apiBaseUrl}/users/${userId}?token=${token}`)
					.then(okOrReject)
					.then(r => r.json())
					.then(user => dispatch(login({ user, token })).payload.user!);
			})
	}, [dispatch]);
}

export function useLogout(): () => Promise<void> {
	const authedFetch = useAuthedFetch();
	const dispatch = useAppDispatch();

	return useCallback(() => {
		return authedFetch(`${env.apiBaseUrl}/logout`, { method: 'POST' })
			.then(okOrReject)
			.then(() => { dispatch(logout()) })
	}, [authedFetch, dispatch]);
}

export function useUpdateProfile(userId?: string): (updated: Partial<User>) => Promise<void> {
	const dispatch = useAppDispatch();
	const authState = useAppSelector(s => s.auth);
	const authedFetch = useAuthedFetch();

	return useCallback((updated: Partial<User>) => {
		const id = userId ?? authState.user?.id;

		return authedFetch(`${env.apiBaseUrl}/users/${id}`)
			.then(okOrReject)
			.then(() => {
				if(id === authState.user?.id)
					dispatch(updateProfile(updated));
			});

	}, [dispatch, authState, authedFetch, userId]);
}
